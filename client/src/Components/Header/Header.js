import React from 'react'
import './Header.css'

export default function Header() {
  return (
    <div className="Header">
        <div className="Header__Titles">
            <span className="Header__Titles_Sm">Blog</span>
            <span className="Header__Titles_Lg">Application</span>
        </div>
        <img
            className="Header_Image"
            src="https://www.springboard.com/blog/wp-content/uploads/2018/10/blog-how-to-get-into-cybersecurity.png"
            alt=""
        />
    </div>
  )
}
