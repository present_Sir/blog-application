import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Context } from '../../Context/Context';
import './TopBar.css';

export default function TopBar() {
  const {user, dispatch} = useContext(Context);
  return (
    <div className="TopBar__Main">
        <div className="TopBar__Left">
            <i className="TopBar__Icon fa-brands fa-twitter"></i>
        </div>
        <div className="TopBar__Center">
            <ul className="TopBar__List">
                <li className="TopBar_Item">
                    <Link className="link" to="/">HOME</Link>
                </li>
                <li className="TopBar_Item">
                    <Link className="link" to="/">ABOUT</Link>
                </li>
                <li className="TopBar_Item">
                    <Link className="link" to="/">CONTACT</Link>
                </li>
                <li className="TopBar_Item">
                    { user  && <Link className="link" to="/write">WRITE</Link> }
                </li>
                <li className="TopBar_Item" onClick={() => dispatch({type: "LOGOUT"})}>
                    {user && "LOGOUT"}
                </li>   
            </ul>
        </div>
        <div className="TopBar__Right">
            {user ? (<img 
                    className="TopBar__Image"
                    src={user.profilePic}
                    alt=""
                />) : (
                    <ul className="TopBar__List">
                        <li className="TopBar_Item"><Link className="link" to="/login">
                            LOGIN
                        </Link></li>
                        <li className="TopBar_Item"><Link className="link" to="/register">
                            REGISTER
                        </Link></li>
                    </ul>
                )
            }
            <i className="TopBar_Search fa-solid fa-magnifying-glass"></i>
        </div>
    </div>
  )
}
