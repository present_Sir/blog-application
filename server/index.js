const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const authRoute = require('./routes/auth');
const usersRoute = require('./routes/users');
const postRoute = require('./routes/posts');
const categoryRoute = require('./routes/categories');
const fileUpload = require('./routes/fileUpload')
// const multer = require('multer');

const app = express();

dotenv.config();

app.use(express.json());

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(console.log("Connected to MONGODB"))
.catch((err) => console.log(err));

// const storage = multer.diskStorage({
//     destination:(req,file,cb) => {
//         cb(null, images)
//     },
//     filename:(req,)
// })

app.use("/api/auth", authRoute);
app.use("/api/user", usersRoute);
app.use("/api/posts", postRoute);
app.use("/api/categories", categoryRoute);
app.use("/api/fileUpload", fileUpload);

app.listen("5000", () => {
    console.log("Server is Running!");
})