import React, { useContext, useEffect, useState } from 'react'
import { useLocation, Link } from 'react-router-dom'
import axios from 'axios';
import './SinglePost.css'
import { Context } from '../../Context/Context';

export default function SinglePost() {
    const path = useLocation().pathname;
    const [post, setPost] = useState({})
    const {user} = useContext(Context);
    useEffect(() => {
        const getPost = async() => {
            const res = await axios.get(`/posts/${path.split('/')[2]}`);
            setPost(res.data);
        }
        getPost();
        // eslint-disable-next-line
    }, [path])

    const handleDelete = async () => {
        const data = {
            username: user.username
        };
        try{
            await  axios.delete(`/posts/${path.split('/')[2]}`, {data}); // axios.delete(`/posts/${path.split('/')[2]}`);
            window.location.replace("/");
        }catch(err){
            console.log(err);
        }
    }

    return (
        <div className="SinglePost">
            <div className="SinglePost_Wrapper">
                <img
                    src={post?.photo ? post.photo: ""}
                    alt=""
                    className="SinglePost_Image"
                />
                
                <h1 className="SinglePost_Title">
                    {post?.title ? post.title : ""}
                    {post?.username === user?.username ? <div className="SinglePost_Title_Edit">
                        <i className="SinglePost_Title_Icon far fa-edit"></i>
                        <i className="SinglePost_Title_Icon far fa-trash-alt"onClick={handleDelete}></i>
                    </div> : null}
                </h1>
                <div className="SinglePost_Info">
                    <Link className="link" to={`/?username=${post?.username ? post.username : null}`}><span className="SinglePost_Author">Author: <b>{post?.username ? post.username : "Anynomus"}</b></span></Link>
                    <span className="SinglePost_Date">{post?.createdAt ? new Date(post.createdAt).toDateString() : ""}</span>
                </div>
                <p className="SinglePost_Desc">
                   {post?.desc ? post.desc : ""}
                </p>
            </div>
        </div>
    )
}
