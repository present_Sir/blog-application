import React, { useRef, useContext } from 'react'
import './Login.css'

import { Link } from 'react-router-dom'
import { Context } from '../../Context/Context';
import axios from 'axios';

export default function Login() {

  const userRef = useRef();
  const passRef = useRef();

  const {user, dispatch, isFetching} = useContext(Context);
  console.log(user);
  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({type: "LOGIN_START"});
    try{
      const res = await axios.post("/auth/login", {
        username: userRef.current.value,
        password: passRef.current.value
      })
      dispatch({type: "LOGIN_SUCCESS",
        payload: res.data
      })
    }catch(err){
      dispatch({type: "LOGIN_FAILURE"})
    }
  }

  return (
      <div className="Login">
        <span className="Login_Title">Login</span>
        <form action="" className="Login_Form" onSubmit={handleSubmit}>
            <label>Username : </label>
            <input ref={userRef} type="text" placeholder='Enter your username ....' />
            <label>Password : </label>
            <input ref={passRef} type="password" placeholder='Enter your password ....' />
            <button className="Login_Login_Button" onClick={handleSubmit}>
                Login
            </button>
            <button className="Login_Register_Button" disabled={isFetching}>
                <Link className="link" to="/register">Register</Link>
            </button>
        </form>
      </div>
  )
}
