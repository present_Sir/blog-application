import {useState, useEffect} from 'react'
import axios from "axios";
import Post from '../Post/Post'
import './Posts.css'
import { useLocation } from 'react-router-dom';

export default function Posts() {

  const [posts, setPosts] = useState([]);
  const {search} = useLocation();

  useEffect(() => {
    const getPosts = async() => {
      const res = await axios.get("/posts"+search);
      console.log(search);
      console.log(res);
      setPosts(res.data);
    }
    getPosts();
    console.log(posts);
    // eslint-disable-next-line
  }, [search])

  return (
    <div className="Posts">
        {posts.map((post, key) => (
          <Post post={post} key={key}/>
        ))}
    </div>
  )
}
