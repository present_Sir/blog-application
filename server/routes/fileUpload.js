

const router = require('express').Router();
const azureStorage = require('azure-storage');
const intoStream = require('into-stream')
const fileUpload = require('express-fileupload')
const dotenv = require('dotenv');

dotenv.config();

router.use(
    fileUpload({
        createParentPath: true
    })
)
const containerName = "blog-app";
const blobService = azureStorage.createBlobService(
    process.env.AZURE_STORAGE_CONNECTION_STRING
)

router.post('/', (req, res) => {
    if(!req.files){
        return res.status(400).json("No Files Received");
    }

    const ar = req.files.file.name.split('.');
    const stream = intoStream(req.files.file.data);
    const blobname = req.body.name + "." + ar.slice(-1)[0];
    const streamLength = req.files.file.data.length;

    blobService.createBlockBlobFromStream(
        containerName,
        blobname,
        stream,
        streamLength,
        (err, _res) => {
            if(err){
                res.status(500);
                res.send({ message: "Error Occured" });    
                return;
            }
            res.status(200).json({ path: `https://blogersapp.blob.core.windows.net/blog-app/${_res.name}` });
            return;
        }
    )
})

module.exports = router;